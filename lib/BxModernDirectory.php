<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Highloadblock as HL;

Loc::loadMessages(__FILE__);

/**
 * @package bx.moderndirectory
 * @author dimabresky
 */
class BxModernDirectory {

    const USER_TYPE = 'moderndirectory';

    /**
     * @var array
     */
    protected static $cache = [];

    /**
     * Returns property type description.
     *
     * @return array
     */
    public static function GetUserTypeDescription() {
        return array(
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => self::USER_TYPE,
            'DESCRIPTION' => Loc::getMessage('BX_MODERNDIRECTORY_PROP_DESC'),
            'ConvertToDB' => [__CLASS__, 'ConvertToDB'],
            'ConvertFromDB' => [__CLASS__, 'ConvertFromDB'],
            'CheckFields' => [__CLASS__, 'CheckFields'],
            'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtmlMulty'),
            'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPublicViewHTML' => array(__CLASS__, 'GetPublicViewHTML'),
            'GetPublicEditHTML' => array(__CLASS__, 'GetPublicEditHTML'),
            'GetPublicEditHTMLMulty' => array(__CLASS__, 'GetPublicEditHTMLMulty'),
            'GetAdminFilterHTML' => array(__CLASS__, 'GetAdminFilterHTML'),
            'GetExtendedValue' => array(__CLASS__, 'GetExtendedValue'),
            'GetSearchContent' => array(__CLASS__, 'GetSearchContent'),
            'AddFilterFields' => array(__CLASS__, 'AddFilterFields'),
            'GetUIFilterProperty' => array(__CLASS__, 'GetUIFilterProperty')
        );
    }

    /**
     * Return public list view html (module list).
     *
     * @param array $arr_property				Property description.
     * @param array $value					Current value.
     * @param array $control		Control description.
     * @return string
     */
    public static function GetPublicViewHTML($arr_property, $value, $control) {
        $rows = self::_directoryItems($arr_property, ['ID' => $value['VALUE']]);
        
        if ($rows[$value['VALUE']]) {
            if (isset($control['MODE']) && 'CSV_EXPORT' == $control['MODE'])
                return $row['ID'];
            elseif (isset($control['MODE']) && ('SIMPLE_TEXT' == $control['MODE'] || 'ELEMENT_TEMPLATE' == $control['MODE']))
                return $row['DISPLAY'];
            else
                return htmlspecialcharsbx($row['DISPLAY']);
        }
        return '';
    }

    /**
     * Returns data for smart filter.
     *
     * @param array $arr_property				Property description.
     * @param array $value					Current value.
     * @return false|array
     */
    public static function GetExtendedValue($arr_property, $value) {
        if (!isset($value['VALUE'])) {
            return false;
        }

        if (is_array($value['VALUE']) && count($value['VALUE']) == 0) {
            return false;
        }

        if (empty($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME'])) {
            return false;
        }

        $rows = self::_directoryItems($arr_property, ['ID' => $value['VALUE']]);

        if (is_array($value['VALUE'])) {
            $result = array();
            foreach ($value['VALUE'] as $val) {
                if (isset($rows[$val])) {
                    $rows[$val]['VALUE'] = $rows[$val]['DISPLAY'];
                    $result[$val] = $rows[$val];
                } else {
                    $result[$val] = false;
                }
            }
            return $result;
        } else {
            if (isset($rows[$value['VALUE']])) {
                $rows[$value['VALUE']]['VALUE'] = $rows[$value['VALUE']]['DISPLAY'];
                return $rows[$value['VALUE']];
            }
        }
        return false;
    }

    /**
     * @param array $arr_property
     * @param array $control
     * @param array &$field
     * @return void
     */
    public static function GetUIFilterProperty($arr_property, $control, &$field) {
        unset($field['value']);
        $items = [];
        foreach (self::_directoryItems($arr_property) as $row) {
            $items[$row['ID']] = $row['DISPLAY'];
        }
        $field['type'] = 'list';
        $field['items'] = $items;
        $field['params'] = ['multiple' => 'Y'];
        $field['operators'] = [
            'default' => '='
        ];
    }

    /**
     * Add values in filter.
     *
     * @param array $arr_property
     * @param array $strHTMLControlName
     * @param array &$arFilter
     * @param bool &$filtered
     * @return void
     */
    public static function AddFilterFields($arr_property, $strHTMLControlName, &$arFilter, &$filtered) {
        $filtered = false;
        $values = array();

        if (isset($_REQUEST[$strHTMLControlName["VALUE"]]))
            $values = (is_array($_REQUEST[$strHTMLControlName["VALUE"]]) ? $_REQUEST[$strHTMLControlName["VALUE"]] : array($_REQUEST[$strHTMLControlName["VALUE"]]));
        elseif (isset($GLOBALS[$strHTMLControlName["VALUE"]]))
            $values = (is_array($GLOBALS[$strHTMLControlName["VALUE"]]) ? $GLOBALS[$strHTMLControlName["VALUE"]] : array($GLOBALS[$strHTMLControlName["VALUE"]]));

        if (!empty($values)) {
            $clearValues = array();
            foreach ($values as $oneValue) {
                $oneValue = (string) $oneValue;
                if ($oneValue != '')
                    $clearValues[] = $oneValue;
            }
            $values = $clearValues;
            unset($oneValue, $clearValues);
        }
        if (!empty($values)) {
            $filtered = true;
            $arFilter['=PROPERTY_' . $arr_property['ID']] = $values;
        }
    }

    /**
     * Return html for public edit value.
     *
     * @param array $arr_property			Property description.
     * @param array $value				Current value.
     * @param array $control			Control description.
     * @return string
     */
    public static function GetPublicEditHTML($arr_property, $value, $control) {
                
        return self::_selectDirectoryRowsHtml($arr_property, [$value['VALUE']], $control, $arr_property['MULTIPLE'] && $arr_property['MULTIPLE'] === 'Y', 'property_filter_select_list');
    }

    /**
     * @param array $arr_property
     * @param array $control
     * @return string
     */
    public static function GetAdminFilterHTML($arr_property, $control) {

        $lAdmin = new CAdminList($control["TABLE_ID"]);
        $lAdmin->InitFilter(array($control["VALUE"]));
        $filterValue = $GLOBALS[$control["VALUE"]];

        return self::_selectDirectoryRowsHtml($arr_property, $filterValue && is_array($filterValue) ? $filterValue : [], $control, true, 'property_filter_multiple');
    }

    /**
     * @param array $arr_property
     * @param array $value
     * @return string
     */
    public static function GetAdminListViewHTML($arr_property, $value) {
        $display = '';
        if ($value['VALUE'] > 0) {
            $rows = self::_directoryItems($arr_property, ['ID' => $value['VALUE']]);
            $row = $rows[$value['VALUE']];
            $display = $rows[$value['VALUE']]['DISPLAY'];
        }
        return $display;
    }

    /**
     * HTML редактирования свойства в форме добавления/редактирования инфоблока
     * @global CUserTypeManager $USER_FIELD_MANAGER
     * @param array $arr_property
     * @param array $value
     * @param array $control
     * @return string
     */
    public static function GetPropertyFieldHtml($arr_property, $value, $control) {

        global $USER_FIELD_MANAGER, $APPLICATION;

        if (!$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) {
            return \ShowError(Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_NOT_SETTED'));
        }

        if ($control['MODE'] === 'iblock_element_admin') {
            // если список элементов инфоблока в админке
            return self::_selectDirectoryRowsHtml($arr_property, $value, $control, false, 'property_field_html_edit_in_admin_list');
        }

        $row = [];

        if ($value['VALUE'] > 0) {
            $ready_data = $USER_FIELD_MANAGER->getUserFieldsWithReadyData('HLBLOCK_' . self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']), self::_directory($arr_property)->getList(['filter' => ['ID' => $value['VALUE']]])->fetch(), LANGUAGE_ID);

            foreach ($ready_data as $fcode => $fdata) {
                $row[$fcode] = $fdata['VALUE'];
            }
            $row["ID"] = $value['VALUE'];
        }

        $mask_labels = self::_maskLabels($arr_property['USER_TYPE_SETTINGS']["MASK_VALUE"]);
        $js_event = 'moderndirectory-' . $arr_property['ID'] . '__row-processed';
        ob_start();
        require __DIR__ . '/../views/property_field_html.php';
        return ob_get_clean();
    }

    /**
     * HTML редактирования свойства в форме добавления/редактирования инфоблока
     * @global CUserTypeManager $USER_FIELD_MANAGER
     * @param array $arr_property
     * @param array $value
     * @param array $control
     * @return string
     */
    public static function GetPropertyFieldHtmlMulty($arr_property, $value, $control) {
        global $USER_FIELD_MANAGER, $APPLICATION;

        if (!$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']) {
            return \ShowError(Loc::getMessage('BX_MODERNDIRECTORY_DIRECTORY_NOT_SETTED'));
        }
        
        if ($control['MODE'] === 'iblock_element_admin') {
            // если список элементов инфоблока в админке
            return self::_selectDirectoryRowsHtml($arr_property, $value, $control, true, 'property_field_html_edit_in_admin_list');
        }

        $rows = [];

        if (!empty($value)) {

            $db_rows = self::_directory($arr_property)->getList(['filter' => ['ID' => array_column($value, "VALUE")]]);

            if ($db_rows) {

                $properties_value_ids = [];
                foreach ($value as $property_value_id => $arr_value) {
                    $properties_value_ids[$arr_value['VALUE']] = $property_value_id;
                }

                while ($row = $db_rows->fetch()) {
                    $ready_data = $USER_FIELD_MANAGER->getUserFieldsWithReadyData('HLBLOCK_' . self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']), $row, LANGUAGE_ID);
                    $rows[$row['ID']] = [
                        'ID' => $row['ID'],
                        'control_name' => "{$control['VALUE']}[{$properties_value_ids[$row['ID']]}]",
                        'is_deleted' => false
                    ];
                    foreach ($ready_data as $fcode => $fdata) {
                        $rows[$row['ID']][$fcode] = $fdata['VALUE'];
                    }
                }
            }
        }

        $mask_labels = self::_maskLabels($arr_property['USER_TYPE_SETTINGS']["MASK_VALUE"]);
        $js_event = 'moderndirectory-' . $arr_property['ID'] . '__row-processed';
        ob_start();
        require __DIR__ . '/../views/property_field_html_multiple.php';
        return ob_get_clean();
    }

    /**
     * Prepare settings for property.
     *
     * @param array $arr_property				Property description.
     * @return array
     */
    public static function PrepareSettings($arr_property) {
        $size = 1;
        $width = 0;
        $multiple = "N";
        $group = "N";
        $directoryTableName = '';
        $mask_value = '';

        if (!empty($arr_property["USER_TYPE_SETTINGS"]) && is_array($arr_property["USER_TYPE_SETTINGS"])) {
            if (isset($arr_property["USER_TYPE_SETTINGS"]["size"])) {
                $size = (int) $arr_property["USER_TYPE_SETTINGS"]["size"];
                if ($size <= 0) {
                    $size = 1;
                }
            }

            if (isset($arr_property["USER_TYPE_SETTINGS"]["width"])) {
                $width = (int) $arr_property["USER_TYPE_SETTINGS"]["width"];
                if ($width < 0) {
                    $width = 0;
                }
            }

            if (isset($arr_property["USER_TYPE_SETTINGS"]["group"]) && $arr_property["USER_TYPE_SETTINGS"]["group"] === "Y") {
                $group = "Y";
            }

            if (isset($arr_property["USER_TYPE_SETTINGS"]["multiple"]) && $arr_property["USER_TYPE_SETTINGS"]["multiple"] === "Y") {
                $multiple = "Y";
            }

            if (isset($arr_property["USER_TYPE_SETTINGS"]["HL_TABLE_NAME"])) {
                $directoryTableName = (string) $arr_property["USER_TYPE_SETTINGS"]['HL_TABLE_NAME'];
            }

            if (isset($arr_property["USER_TYPE_SETTINGS"]["MASK_VALUE"])) {
                $mask_value = (string) $arr_property["USER_TYPE_SETTINGS"]['MASK_VALUE'];
            }
        }

        $result = array(
            'size' => $size,
            'width' => $width,
            'group' => $group,
            'multiple' => $multiple,
            'HL_TABLE_NAME' => $directoryTableName,
            'MASK_VALUE' => $mask_value
        );

        $arr_property['USER_TYPE_SETTINGS'] = $result;
        $arr_property['DEFAULT_VALUE'] = '';

        return $arr_property;
    }
    
    /**
     * @global CUserTypeManager $USER_FIELD_MANAGER
     * @param array $arr_property
     * @param array $control_name
     * @param array $property_fields
     * @return array
     */
    public static function GetSettingsHTML($arr_property, $control_name, &$property_fields) {
        global $USER_FIELD_MANAGER;
        $property_fields = [
            'HIDE' => ['ROW_COUNT', 'COL_COUNT', 'MULTIPLE_CNT', 'DEFAULT_VALUE', 'WITH_DESCRIPTION', 'SEARCHABLE'],
            'SET' => ['DEFAULT_VALUE' => '']
        ];
        $arr_property = self::PrepareSettings($arr_property);
        ob_start();
        require __DIR__ . '/../views/settings.php';
        return ob_get_clean();
    }

    /**
     * @param array $arr_property
     * @param array $value
     * @return array
     */
    public static function CheckFields($arr_property, $value) {
        $errors = [];

        return $errors;
    }

    /**
     * @param array $arr_property
     * @param array $value
     * @return array
     */
    public static function ConvertToDB($arr_property, $value) {

        return $value;
    }

    /**
     * @param array $arr_property
     * @param array $value
     * @return array
     */
    public static function ConvertFromDB($arr_property, $value) {

        return $value;
    }

    /**
     * @param string $table_name
     * @return type
     */
    public static function _directoryEntityId($table_name) {

        if (!isset(self::$cache[$table_name])) {
            self::$cache[$table_name] = [];
        }

        if (!isset(self::$cache[$table_name]['TABLE_ID'])) {
            $iterator = HL\HighloadBlockTable::getList([
                        'select' => ['ID'],
                        'filter' => ['=TABLE_NAME' => $table_name]
            ]);
            $row = $iterator->fetch();
            self::$cache[$table_name]['TABLE_ID'] = intval($row['ID']);
        }


        return self::$cache[$table_name]['TABLE_ID'];
    }

    /**
     * @param array $arr_property
     * @throws Exception
     * return \Bitrix\Main\Entity
     */
    protected static function _directoryEntity($arr_property) {

        if (!isset(self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']])) {
            self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']] = [];
        }

        if (!isset(self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY_ENTITY'])) {
            self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY_ENTITY'] = HL\HighloadBlockTable::compileEntity($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']);
        }

        if (!self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY_ENTITY']) {
            throw new Exception(Loc::getMessage("BX_MODERNDIRECTORY_DIRECTORY_NOT_FOUND", ["#CODE#" => $arr_property['CODE']]));
        }

        return self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY_ENTITY'];
    }

    /**
     * @param array $arr_property
     * return \Bitrix\Main\Entity\DataManager
     */
    protected static function _directory($arr_property) {

        if (!isset(self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']])) {
            self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']] = [];
        }

        if (!isset(self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY'])) {
            $class_name = self::_directoryClass($arr_property);
            self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY'] = new $class_name;
        }

        return self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['DIRECTORY'];
    }

    /**
     * @param array $arr_property
     * @return string
     */
    protected static function _directoryClass($arr_property) {

        $entity = self::_directoryEntity($arr_property);
        return $entity->getDataClass();
    }

    /**
     * @param array $arr_property
     * @param array $row
     * @return int
     * @throws Exception
     */
    protected static function _processingDirectoryRow($arr_property, $row) {

        global $USER_FIELD_MANAGER;

        $directory = self::_directory($arr_property);

        $fields = $row;

        unset($fields['ID']);
        unset($fields['KEY']);

        $USER_FIELD_MANAGER->EditFormAddFields('HLBLOCK_' . self::_directoryEntity($arr_property)->getId(), $fields, ['FORM' => $_REQUEST["PROPERTY_{$arr_property['ID']}"], 'FILES' => $_FILES["PROPERTY_{$arr_property['ID']}"]]);

        if (isset($row['ID']) && $row['ID'] > 0) {

            // обновление записи справочника
            $result = $directory->update($row['ID'], $fields);
        } else {

            // создание записи справочника
            $result = $directory->add($row['ID'], $fields);
        }
        if ($result->isSuccess()) {
            return $result->getId();
        }
        throw new Exception(implode("<br>", $result->getErrorMessages()));
    }

    /**
     * @param array $arr_property
     * @param string $key
     * @param int $value
     */
    protected static function _saveConvertedValueInCache($arr_property, $key, $value) {
        if (!isset(self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']])) {
            self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']] = [];
        }

        if (!isset(self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['CONVERTED_VALUES'])) {
            self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['CONVERTED_VALUES'] = [];
        }

        self::$cache[$arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']]['CONVERTED_VALUES'][$key] = $value;
    }

    /**
     * @return array
     */
    protected static function _directoryList() {
        $rows = HL\HighloadBlockTable::getList(array(
                    'select' => array('*', 'NAME_LANG' => 'LANG.NAME'),
                    'order' => array('NAME_LANG' => 'ASC', 'NAME' => 'ASC')
        ));
        $list = [];
        while ($row = $rows->fetch()) {
            $list[$row['TABLE_NAME']] = $row['NAME_LANG'] ? $row['NAME_LANG'] : $row['TABLE_NAME'];
        }
        return $list;
    }

    /**
     * @return string
     */
    protected function _moduleRoot() {
        if (file_exists(\Bitrix\Main\Application::getDocumentRoot() . "/bitrix/modules/bx.moderndirectory/")) {
            return "/bitrix";
        } else {
            return "/local";
        }
    }

    /**
     * @param string $mask
     * @return array
     */
    protected function _maskLabels($mask) {
        if (!$mask) {
            return [];
        }

        $matches = [];
        preg_match_all("#(ID)|(UF_[A-Z]+)#", $mask, $matches);
        return !empty($matches[0]) ? $matches[0] : [];
    }

    /**
     * @param array $arr_property
     * @return int
     */
    protected function _directoryRowsCount(array $arr_property) {
        return intval(self::_directory($arr_property)->getList([
                    'select' => [new ExpressionField('CNT', 'COUNT(1)')]
                ])->fetch()['CNT']);
    }

    /**
     * @staticvar array $mask_labels
     * @param array $row
     * @param array $arr_property
     * @return string
     */
    protected function _displayValue($row, $arr_property) {
        static $mask_labels = [];
        if (!$mask_labels) {
            $mask_labels = self::_maskLabels($arr_property['USER_TYPE_SETTINGS']["MASK_VALUE"]);
        }
        if (!empty($mask_labels)) {
            $arr = [];
            foreach ($mask_labels as $label) {
                $arr["#" . $label . "#"] = $row[$label];
            }
            $display = str_replace(array_values(array_keys($arr)), array_values($arr), $arr_property['USER_TYPE_SETTINGS']["MASK_VALUE"]);
        } elseif ($row['UF_NAME_' . LANGUAGE_ID]) {
            $display = $row['UF_NAME_' . LANGUAGE_ID];
        } elseif ($row['UF_NAME']) {
            $display = $row['UF_NAME'];
        } else {
            $display = GetMessage('BX_MODERNDIRECTORY_ROW_TITLE') . " #{$row['ID']}";
        }
        return $display;
    }

    /**
     * @param array $arr_property
     * @param midex $values
     * @param array $control
     * @param bool $is_multiple
     * @param string $file_name
     * @return string
     */
    protected function _selectDirectoryRowsHtml($arr_property, $values, $control, $is_multiple = false, $file_name) {

        $rows = self::_directoryItems($arr_property);

        if (!empty($rows)) {
            ob_start();
            require __DIR__ . '/../views/' . $file_name . '.php';
            return ob_get_clean();
        }

        return '';
    }

    /**
     * @global CUserTypeManager $USER_FIELD_MANAGER
     * @param array $arr_property
     * @param array $filter
     * @return array
     */
    protected function _directoryItems($arr_property, $filter = null) {
        global $USER_FIELD_MANAGER;
        if (!$filter) {
            $filter = [];
        }
        $db_rows = self::_directory($arr_property)->getList(['filter' => $filter]);
        $rows = [];
        while ($row = $db_rows->fetch()) {

            $ready_data = $USER_FIELD_MANAGER->getUserFieldsWithReadyData('HLBLOCK_' . self::_directoryEntityId($arr_property['USER_TYPE_SETTINGS']['HL_TABLE_NAME']), $row, LANGUAGE_ID);
            foreach ($ready_data as $fcode => $fdata) {
                if (is_array($fdata['VALUE'])) {
                    $row[$fcode] = implode(", ", array_filter($fdata['VALUE'], function ($v) {return !empty(trim($v));}));
                } else {
                    $row[$fcode] = $fdata['VALUE'];
                }
            }
            
            $row['DISPLAY'] = self::_displayValue($row, $arr_property);
            $rows[$row['ID']] = $row;
        }

        return $rows;
    }

}
