<?php
/**
 * @var BxModernDirectory self
 * @var array $arr_property
 * @var array $control
 * @var boolean $is_multiple
 */
?>
<div class="moderndirectory">
<? if ($is_multiple): ?>
    <? if (!empty($values)): ?>
        <?
        foreach ($values as $property_value_id => $value):
            if ($property_value_id <=0) {
                continue;
            }
            ?>
            <select name="<?= $control['VALUE'] ?>[<?= $property_value_id ?>][VALUE]">
                <option value=""><?= GetMessage('BX_MODERNDIRECTORY_ROW_NOT_SETTED_TITLE') ?></option>
                <? foreach ($rows as $id => $row): ?>
                    <option <? if ($value['VALUE'] == $id): ?>selected<? endif ?> value="<?= $id ?>"><?= $row['DISPLAY'] ?></option>
                <? endforeach ?>
            </select><br>
        <? endforeach; ?>
    <? endif ?>
    <select name="<?= $control['VALUE'] ?>[n1][VALUE]">
        <option value=""><?= GetMessage('BX_MODERNDIRECTORY_ROW_NOT_SETTED_TITLE') ?></option>
        <? foreach ($rows as $id => $row): ?>
            <option value="<?= $id ?>"><?= $row['DISPLAY'] ?></option>
        <? endforeach; ?>
    </select><br>
    <input value="Еще" onclick="add_moderndirectory_row(event)" data-next-index="2" type="button">
    <?
    ob_start();
    ?>
    <script>
        (() => {
            
            if (!window.add_moderndirectory_row) {
                window.add_moderndirectory_row = (e) => {
                    let btn = e.target;
                    let nextIndex = btn.dataset.nextIndex;
                    let select = document.createElement('select');
                    let option = document.createElement('option');
                    select.name = `<?= $control['VALUE'] ?>[n${nextIndex}][VALUE]`;
                    option.value = "";
                    option.innerText = '<?= GetMessage('BX_MODERNDIRECTORY_ROW_NOT_SETTED_TITLE') ?>';
                    select.appendChild(option);
                    <?$cnt = 0; foreach ($rows as $id => $row): ++$cnt;?>
                        let option_<?= $cnt?> = document.createElement('option');
                        option_<?= $cnt?>.value = '<?= $id?>';
                        option_<?= $cnt?>.innerText = '<?= $row['DISPLAY']?>';
                        select.appendChild(option_<?= $cnt?>);
                    <? endforeach; ?>
                    btn.parentNode.insertBefore(select, btn);
                    btn.parentNode.insertBefore(document.createElement('br'), btn);
                    btn.dataset.nextIndex = nextIndex + 1;
                };
            }
        })();
    </script>
    <?
    Bitrix\Main\Page\Asset::getInstance()->addString(ob_get_clean());
else:
    ?>
    <select name="<?= $control['VALUE'] ?>">
        <option value=""><?= GetMessage('BX_MODERNDIRECTORY_ROW_NOT_SETTED_TITLE') ?></option>
        <? foreach ($rows as $id => $row): ?>
            <option <? if (@$values['VALUE'] == $id): ?>selected<? endif ?> value="<?= $id ?>"><?= $row['DISPLAY'] ?></option>
        <? endforeach ?>
    </select>
<? endif; ?>
</div>

